package lab1

fun main() {
    with(IntegrationData()){
        runEuler(this)
        afterRun()
        runSimpson(this)
        afterRun()
        runGauss(this)
    }
}

fun afterRun() = println("-----------------------------------")

fun runGauss(integrationData: IntegrationData) = println("GAUSS VALUE: ${integrationData.gauss()}")

fun runSimpson(integrationData: IntegrationData) {
    println("SIMPSON:")
    runAllSteps(SimpsonInt(integrationData))
}

fun runEuler(integrationData: IntegrationData) {
    println("EULER:")
    runAllSteps(EulerInt(integrationData))
}

fun runAllSteps(int: PartialIntegration) =
    listOf(0.1, 0.05, 0.025).forEach { printStep(int.withStep(it)) }

fun printStep(integration: PartialIntegration) =
    println("STEP = ${integration.data.step}; INTEGRAL VALUE = ${integration.integrate()}; RUDGE = ${integration.rudge()}")