package lab1

import splitInterval
import kotlin.math.pow
import kotlin.math.sqrt

class EulerInt(data: IntegrationData) : PartialIntegration(data) {
    override fun integrate(): Double {
        val x = splitInterval()
        return with(data) {
            //@formatter:off
            (step / 2 * (f(x.first()) + 2 * x.slice(1..x.size - 2).sumOf { f(it) } + f(x.last())))+
                    (step.pow(2) / 12 * (df(start) - df(stop)))
            //@formatter:on
        }
    }

    override fun withStep(step: Double): PartialIntegration = EulerInt(data.copy(step = step))

    override val rudgeCoef: Int
        // вроде тоже правда, т.е. совпадает с формулой эйлера
        get() = 15
}

class SimpsonInt(data: IntegrationData) : PartialIntegration(data) {
    override fun integrate(): Double {
        val x = splitInterval()
        return with(data) {
            step / 6 * (0..x.size - 2).sumOf { i ->
                f(x[i]) + 4 * f((x[i] + x[i + 1]) / 2) + f(x[i + 1])
            }
        }
    }

    override fun withStep(step: Double): PartialIntegration = SimpsonInt(data.copy(step = step))

    override val rudgeCoef: Int
        // точно правда
        get() = 15
}

abstract class PartialIntegration(
    val data: IntegrationData
) {
    protected fun splitInterval(): List<Double> = with(data) {
        splitInterval(start, stop, step)
    }

    abstract fun integrate(): Double

    abstract fun withStep(step: Double): PartialIntegration

    abstract val rudgeCoef: Int

    fun rudge(): Double = (integrate() - withStep(data.step * 2).integrate()) / rudgeCoef
}

data class IntegrationData(
    val start: Double = 1.0,
    val stop: Double = 2.0,
    val step: Double = 0.1,
    val f: (x: Double) -> Double =  { x -> 1 / (x.pow(4) + 1) },
    val df: (x: Double) -> Double = { x -> -4 * x.pow(3) / (1 + x.pow(4)).pow(2) },
    private val gaussF: (x: Double) -> Double = { x -> 8 / ((x + 3).pow(4) + 16) }
) {
    fun gauss(): Double = gaussF(-1 / sqrt(3.0)) + gaussF(1 / sqrt(3.0))
}
