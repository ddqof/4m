fun splitInterval(start: Double, end: Double, step: Double) : List<Double> =
    generateSequence(start) { "%.10f".format(it + step).toDouble() }
        .takeWhile { it <= end }
        .toList()