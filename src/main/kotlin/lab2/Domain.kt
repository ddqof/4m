package lab2

import splitInterval
import kotlin.math.pow

typealias DifferEqFormula = (Double, Double) -> Double

abstract class CauchyProblemData {
    abstract val f: DifferEqFormula
    abstract val dfdx: DifferEqFormula
    abstract val dfdy: DifferEqFormula
    abstract val accurateSolution: (Double) -> Double
    abstract val y0: Double
    abstract val n: Int
    private val x0: Double = 0.0
    private val xn: Double = 1.0
    val h: Double by lazy { (xn - x0) / n }
    val x: List<Double> by lazy { splitInterval(x0, xn, h) }
}

class EulerExplicit(data: CauchyProblemData) : DifferEqSolvingMethod(data) {
    override fun step(y: List<Double>, i: Int): Double =
        with(data) { y[i] + f(x[i], y[i]) * h }

    override val name: String = "Метода Эйлера явный"
}

class CauchyMethod(data: CauchyProblemData) : DifferEqSolvingMethod(data) {
    override fun step(y: List<Double>, i: Int): Double =
        with(data) { y[i] + h * f(x[i] + h / 2, y[i] + (h / 2 * f(x[i], y[i]))) }

    override val name: String = "Метод Коши"
}

class Taylor3Method(data: CauchyProblemData) : DifferEqSolvingMethod(data) {
    override fun step(y: List<Double>, i: Int): Double {
        return with(data) {
            y[i] + f(x[i], y[i]) * h + h.pow(2) / 2 *
                    (dfdx(x[i], y[i]) + dfdy(x[i], y[i]) * f(x[i], y[i]))
        }
    }

    override val name: String = "Метод Тейлора третьего порядка"
}

abstract class DifferEqSolvingMethod(val data: CauchyProblemData) : SolutionMethod() {
    override fun solve(): List<Double> = with(data) {
        x.dropLast(1).indices.fold(listOf(y0)) { y: List<Double>, i: Int -> y.plus(step(y, i)) }
    }

    abstract fun step(y: List<Double>, i: Int): Double
}

class AccurateSolution(val data: CauchyProblemData) : SolutionMethod() {
    override val name: String = "Точное решение"

    override fun solve(): List<Double> = with(data) { x.map { accurateSolution(it) } }
}

abstract class SolutionMethod {
    abstract val name: String
    abstract fun solve(): List<Double>
}