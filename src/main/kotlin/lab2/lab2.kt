package lab2

import kotlin.math.exp
import kotlin.math.pow

fun main(args: Array<String>) {
    with(Variant11(if (args.isNotEmpty()) args.first().toInt() else 14)) {
        println(x.size)
        listOf(
            EulerExplicit(this),
            CauchyMethod(this),
            Taylor3Method(this),
            AccurateSolution(this)
        ).map { SolutionResult(it.name, it.solve()) }
            .forEach { println(it) }
    }
}

class Variant11(override val n: Int) : CauchyProblemData() {
    override val f: DifferEqFormula = { x, y ->
        50 * y * (x - 0.6) * (x - 0.85)
    }
    override val accurateSolution: (Double) -> Double = { x ->
        exp(50 * (x.pow(3) / 3 - 0.725 * x.pow(2) + 0.51 * x)) * c
    }
    override val dfdx: DifferEqFormula = { x, y ->
        (100 * x - 72.5) * y
    }
    override val dfdy: DifferEqFormula = { x, y ->
        50 * (x - 0.85) * (x - 0.6)
    }
    override val y0: Double = 0.1
    val c: Double = 0.1
}

data class SolutionResult(
    val methodName: String,
    val y: List<Double>
) {
    override fun toString(): String = "$methodName: $y"
}